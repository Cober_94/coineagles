<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profile extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('Lon_In_model');
		$this->load->model('processes_model');
	}
	public function index(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['Usuarios'] = $this->Lon_In_model->Profile($My_Id);
				$data['dashboard'] = "Profile";
				$this->load->view('dashboard/vendor/head');
				$this->load->view('dashboard/vendor/menu',$data);
				$this->load->view('dashboard/vendor/menu2');
				$this->load->view('dashboard/profile');
				$this->load->view('dashboard/vendor/script');
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_admin =  $user->id_user;
				$data['admin'] = $this->Lon_In_model->Profiles($My_admin);
				$My_Id = $this->uri->segment(3);
				$data['My_Id'] = $My_Id;
				$data['Usuarios'] = $this->Lon_In_model->Profile($My_Id);
				$data['de'] = $this->processes_model->transaccion_de($My_Id);
				$data['a'] = $this->processes_model->transaccion_a($My_Id);
				$data['dashboard'] = "Profile";
				$this->load->view('admin_dashboard/vendor/head');
				$this->load->view('admin_dashboard/vendor/menu',$data);
				$this->load->view('admin_dashboard/vendor/menu2');
				$this->load->view('admin_dashboard/profile');
				$this->load->view('admin_dashboard/vendor/script');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
}
}
