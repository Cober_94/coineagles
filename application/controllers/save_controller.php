<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class save_controller extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('Lon_In_model');
		$this->load->model('processes_model');
		$this->load->model('save_model');
	}
public function Emails(){
	if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {

				$usuario = $this->session->userdata('mail');
				$user = $this->Lon_In_model->datos($usuario);
				$My_admin =  $user->id_user;

				//libreria para enviar correos
				$this->load->library('email');

				$destino = $this->input->post('destino');
				$asunto = $this->input->post('asunto');
				$mensaje = $this->input->post('mensaje');

				$config['mailtype'] = '*';
				//quien lo manda
 				$this->email->from('Support@coineagle.com', 'Coineagle.Info');
 				//quien lo recibe
				$this->email->to($destino);
				//asunto de correo
				$this->email->subject($asunto);
				//mensaje
				$this->email->message($mensaje);
				//es el que nos permite enviar el correo
				$this->email->send();
				
				$data['correo'] =$destino;
				$data['asunto'] =$asunto;
				$data['mail'] =$mensaje;
				$data['id_user'] =$My_admin;
				$data['estado'] = 0;
				$data['fecha'] = date("Y-m-d");
				$data['hora'] = date("h:i:s"); 

				$this->save_model->mail($data);
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
}