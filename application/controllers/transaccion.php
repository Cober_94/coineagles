<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transaccion extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('transaccion_model');
		$this->load->model('Lon_In_model');
		$this->load->model('NewAccount_model');
	}
	public function transaccion(){
		$usuario = $this->session->userdata('mail');
		$user = $this->Lon_In_model->datos($usuario);
		$My_Id =  $user->id_user;

		$system = $_POST['system'];
		$Templates = $_POST['templates'];
		$address = $_POST['address'];
		$Amount = $_POST['Amount'];
		$suma = ($Amount * 0.01);
		$total = $Amount + $suma;
			
			$cartera = $this->transaccion_model->system($system);
			$c = $cartera->ID;
			if ($cartera = NULL) {
					$this->session->set_flashdata('error','Porfavor no manipule el html');
					redirect('coineagle/transactions');
			}else{
				if($system == NULL or $address == NULL or $Amount == NULL){
					$this->session->set_flashdata('error','Un campo es nulo');
					redirect('coineagle/transactions');
				}else{
					$data['id_usuario'] = $My_Id;
					$data['Choose_system'] = $c;
					//$data['Templates'] = $Templates;
					$data['address'] = $address;
					$data['Amount'] = $Amount;
					$data['total'] = $total;
					$data['Comission'] = $suma;
					$data['fecha'] = date("Y-m-d");
					$data['hora'] = date("H:i:s");
					$config = $this->transaccion_model->ingreso($data);
					if($config = True){
						$this->session->set_flashdata('success', 'Su Solicitud fue aceptada has la verificacion desde tu correo');
						redirect('coineagle/transactions');
					}else{
						$this->session->set_flashdata('error', 'Ocurrio un Problema');
						redirect('coineagle/transactions');
					}
				}
			}
	}
	public function Type_sistem(){
		$usuario = $this->session->userdata('mail');
		$user = $this->Lon_In_model->datos($usuario);
		$My_Id =  $user->id_user;

		$Type_sistem = $_POST['system'];
		if ($Type_sistem == 0) {
			$this->transaccion_model->BTC($My_id);
		}elseif($Type_sistem == 0) {
			$this->transaccion_model->LTC($My_id);
		}elseif($Type_sistem == 0) {
			$this->transaccion_model->ETC($My_id);
		}else{
			echo "error";
		}
	}
	public function Amount(){
		
		$usuario = $this->session->userdata('mail');
		$user = $this->Lon_In_model->datos($usuario);
		$My_Id =  $user->id_user;
		$Amount = $this->input->post('Amount');
		$system = $this->input->post('system');
		if ($system == "BITCOIN") {
			$data = $this->transaccion_model->monto_btc($My_Id);
			if ($Amount <= $data->Monto) {
				echo 1;
			}else{
				echo 2;
			}
		}elseif ($system <= "ETHEREUM") {
			$data = $this->transaccion_model->monto_eth($My_Id);
			if ($Amount <= $data->Monto) {
				echo 1;
			}else{
				echo 2;
			}
		}elseif ($system == "LITECOIN") {
			$data = $this->transaccion_model->monto_ltc($My_Id);
			if ($Amount <= $data->Monto) {
				echo 1;
			}else{
				echo 2;
			}
		}else{
			echo "error";
		}
	}
	public function system(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo $this->input->post('system');
			}elseif ($this->session->userdata('rol') == '2') {
				echo "usted no tiene los permisos";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
	public function account_tow(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo $this->input->post('account_tow');
			}elseif ($this->session->userdata('rol') == '2') {
				echo "usted no tiene los permisos";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
	public function two(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
					
				$system = $this->input->post('system');

				if ($system == 'BITCOIN') {
			
				    $data_btc = $this->input->post('Amount');
					$moneda_b = 7774.79;
					echo $this->NewAccount_model->calculadora_BTC($data_btc,$moneda_b);
					
				}elseif ($system == 'LITECOIN') {
			
					$data_eth = $this->input->post('Amount');
					$moneda_e = 237.80;
					echo $this->NewAccount_model->calculadora_ETH($data_eth,$moneda_e);

				}elseif ($system == 'ETHEREUM') {

					$data_lth = $this->input->post('Amount');
					$moneda_l = 116.27;
					echo $this->NewAccount_model->calculadora_LTH($data_lth,$moneda_l);
				}else{
					echo "Por favor no modifique el codigo";
				}

			}elseif ($this->session->userdata('rol') == '2') {
				echo "usted no tiene los permisos";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
	public function mont(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$account_tow = $this->input->post('account_tow');

				if ($account_tow == 'BITCOIN') {
			
				    $data_btc = $this->input->post('Amount');
					$moneda_b = 7774.79;
					echo $this->NewAccount_model->calculadora_BTC($data_btc,$moneda_b);
					
				}elseif ($account_tow == 'LITECOIN') {
			
					$data_eth = $this->input->post('Amount');
					$moneda_e = 237.80;
					echo $this->NewAccount_model->calculadora_ETH($data_eth,$moneda_e);

				}elseif ($account_tow == 'ETHEREUM') {

					$data_lth = $this->input->post('Amount');
					$moneda_l = 116.27;
					echo $this->NewAccount_model->calculadora_LTH($data_lth,$moneda_l);
				}else{
					echo "Por favor no modifique el codigo";
				}
			}elseif ($this->session->userdata('rol') == '2') {
				echo "usted no tiene los permisos";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}		
}