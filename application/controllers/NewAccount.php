<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewAccount extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('NewAccount_model');
	}
	public function index()
	{
		$this->load->view('vendor/head');
		$this->load->view('welcome_message');
		$this->load->view('vendor/script');
	}
	public function save()
	{
		$longitud = 3;
		$num = rand(0,9);

		for ($i=0; $i <= $longitud ; $i++) { 
			$numero = rand(0,9);
			 $num.=$numero;
		}

		$data['nombre'] = $_POST['user_name'];
		$data['apellidos'] = $_POST['last_name'];
		$data['correo'] = $_POST['email'];
		$data['teelefono'] = $_POST['phone'];
		$data['fecha'] = date("Y-m-d");
		$data['fecha_nacimiento'] = $_POST['Birthdate'];
		$data['pass'] = $_POST['password'];
		$data['rol'] = 1;
		$data['estado'] = 0;
		if ($data['nombre'] == NULL or $data['apellidos'] == NULL or $data['correo'] == NULL or $data['teelefono'] == NULL or $data['fecha_nacimiento'] == NULL or $data['pass'] == NULL) {
			echo "ERROR un campo no a sido llenado";
		}else{
			//pin 
				$data['pin'] = $num."-".$numero;

				$asunto = "verificacion de cuenta";
				$mensaje = "hola";
				$config['mailtype'] = '*';
						//quien lo manda
		 				$this->email->from('Support@coineagle.com', 'Coineagle.Info');
		 				//quien lo recibe
						$this->email->to($data['correo']);
						//asunto de correo
						$this->email->subject($asunto);
						//mensaje
						$this->email->message($mensaje);
						//es el que nos permite enviar el correo
						//$this->email->send();
				echo $this->NewAccount_model->Save_User($data);
		}
	}

}
