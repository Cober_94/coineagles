<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class coineagle extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('Lon_In_model');
		$this->load->model('processes_model');
		$this->load->model('NewAccount_model');
	}
	public function index(){
		
		$this->load->view('vendor/head');
		$this->load->view('welcome_message');
		$this->load->view('vendor/script');

	}
	public function login()
	{
		$this->load->view('vendor/head');
		$this->load->view('login');
		$this->load->view('vendor/script');
	}
	public function Home()
	{	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$My_cuenta = $this->Lon_In_model->validar_cuenta($My_Id);

				if ($My_cuenta->estado == 0) {
					
					$this->load->view('vendor/head');
					$this->load->view('dashboard/validar_cuenta');
					$this->load->view('vendor/script');

				}elseif($My_cuenta->estado == 1){
					$data['percentage'] = 0;
					$data['Usuarios'] = $this->Lon_In_model->perfil($My_Id);
					$data['dashboard'] = "dashboard";

						 $btc = $this->processes_model->btc($My_Id);
						 $ltc = $this->processes_model->ltc($My_Id);
						 $eth = $this->processes_model->eth($My_Id);

						//toman el valor de las cuentas del usuario//
						 $BITCOIN = $btc->Monto;
						 $LITECOIN =  $ltc->Monto;
						 $ETHEREUM = $eth->Monto;
						//nos dan el tipo de cuenta de cada usuario//
						 $porceng_btc = $this->NewAccount_model->porcentage_BTC($BITCOIN);
						 $porceng_ltc = $this->NewAccount_model->porcentage_LTH($LITECOIN);
						 $porceng_eth = $this->NewAccount_model->porcentage_ETH($ETHEREUM);
						 
			//--------------------------------------calculo BTC-----------------------------------//
						    $data_btc = $btc->Monto;
							$moneda_b = 7774.79;
					$data['btc_s'] = $this->NewAccount_model->calculadora_BTC($data_btc,$moneda_b);
			//--------------------------------------calculo ETH-----------------------------------//
							$data_eth = $eth->Monto;
							$moneda_e = 237.80;
					$data['btc_e'] = $this->NewAccount_model->calculadora_ETH($data_eth,$moneda_e);
			//-------------------------------------calculo LTH------------------------------------//
							$data_lth = $ltc->Monto;
							$moneda_l = 116.27;
					$data['btc_l'] = $this->NewAccount_model->calculadora_LTH($data_lth,$moneda_l);
			//------------------------------------------END---------------------------------------//
						 
					//estas se mandan a imprimir a la vista por medio de la data// 
					 $data['btc'] = $BITCOIN;
					 $data['lth'] = $LITECOIN; 
					 $data['eth'] = $ETHEREUM;
					//estas se mandan a imprimir a la vista por medio de la data//
					 $data['beneficio_btc'] = $porceng_btc;
					 $data['beneficio_eth'] = $porceng_eth;
					 $data['beneficio_lth'] = $porceng_ltc;
					//suma todo los porcentajes de cada cuenta para sacar un total//
					 $data['Porcentage'] = ($porceng_eth + $porceng_btc + $porceng_ltc);

					$this->load->view('dashboard/vendor/head',$data);
					$this->load->view('dashboard/vendor/menu');
					$this->load->view('dashboard/vendor/menu2');
					$this->load->view('dashboard/dashboard');
					$this->load->view('dashboard/vendor/script');
				}else{
					echo"su cuenta esta siendo exsaminda";
				}

			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['admin'] = $this->Lon_In_model->Profile($My_Id);
				$data['dashboard'] = "Dashboard";
				$this->load->view('admin_dashboard/vendor/head');
				$this->load->view('admin_dashboard/vendor/menu',$data);
				$this->load->view('admin_dashboard/vendor/menu2');
				$this->load->view('admin_dashboard/dashboard');
				$this->load->view('admin_dashboard/vendor/script');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function transactions()
	{	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['Usuarios'] = $this->Lon_In_model->perfil($My_Id);
				$data['dashboard'] = "Transactions";
				$this->load->view('dashboard/vendor/head',$data);
				$this->load->view('dashboard/vendor/menu');
				$this->load->view('dashboard/vendor/menu2');
				$this->load->view('dashboard/transactions');
				$this->load->view('dashboard/vendor/script');

			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_admin =  $user->id_user;
				$My_Id = $this->uri->segment(3);
				$transactions = $this->uri->segment(4);
				$data['admin'] = $this->Lon_In_model->Profiles($My_admin);
				$data['Usuarios'] = $this->Lon_In_model->Profile($My_Id);
				if ($My_Id == NULL) {
					$data['dea'] = $this->processes_model->tables_transacciones();
							$data['dashboard'] = "transactions echa por los usuario";
							$this->load->view('admin_dashboard/vendor/head',$data);
							$this->load->view('admin_dashboard/vendor/menu');
							$this->load->view('admin_dashboard/vendor/menu2');
							$this->load->view('admin_dashboard/transactions');
							$this->load->view('admin_dashboard/vendor/script');
				}else{
					if ($transactions == 1) {
							$data['dea'] = $this->processes_model->tables_process_de($My_Id);
							$data['dashboard'] = "transactions echa por el usuario";
							$this->load->view('admin_dashboard/vendor/head');
							$this->load->view('admin_dashboard/vendor/menu',$data);
							$this->load->view('admin_dashboard/vendor/menu2');
							$this->load->view('admin_dashboard/transactions');
							$this->load->view('admin_dashboard/vendor/script');
					}elseif($transactions == 2){
							$data['dea'] = $this->processes_model->tables_process_a($My_Id);
							$data['dashboard'] = "transactions echa al Usuario";
							$this->load->view('admin_dashboard/vendor/head');
							$this->load->view('admin_dashboard/vendor/menu',$data);
							$this->load->view('admin_dashboard/vendor/menu2');
							$this->load->view('admin_dashboard/transactions');
							$this->load->view('admin_dashboard/vendor/script');
					}else{
						echo "no toque el codigo";
					}
				}			
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function accounts()
	{	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['Usuarios'] = $this->Lon_In_model->perfil($My_Id);
				$data['dashboard'] = "0.0000000";
				$this->load->view('dashboard/vendor/head');
				$this->load->view('dashboard/vendor/menu',$data);
				$this->load->view('dashboard/vendor/menu2');
				$this->load->view('dashboard/accounts');
				$this->load->view('dashboard/vendor/script');			
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['admin'] = $this->Lon_In_model->Profile($My_Id);
				$data['dashboard'] = "accounts";
				$this->load->view('admin_dashboard/vendor/head');
				$this->load->view('admin_dashboard/vendor/menu',$data);
				$this->load->view('admin_dashboard/vendor/menu2');
				$this->load->view('admin_dashboard/accounts');
				$this->load->view('admin_dashboard/vendor/script');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
//-------------------------------------controller admint---------------------------------------------------
	public function users()
	{	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['admin'] = $this->Lon_In_model->Profile($My_Id);
				$data['dashboard'] = "Transactions";
				$this->load->view('admin_dashboard/vendor/head');
				$this->load->view('admin_dashboard/vendor/menu',$data);
				$this->load->view('admin_dashboard/vendor/menu2');
				$this->load->view('admin_dashboard/registered_users');
				$this->load->view('admin_dashboard/vendor/script');				
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function Email(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['admin'] = $this->Lon_In_model->Profile($My_Id);
				$data['dashboard'] = "Transactions";
				$this->load->view('admin_dashboard/vendor/head');
				$this->load->view('admin_dashboard/vendor/menu',$data);
				$this->load->view('admin_dashboard/vendor/menu2');
				$this->load->view('admin_dashboard/email');
				$this->load->view('admin_dashboard/vendor/script');				
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function Enviados(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$data['email'] = $this->processes_model->email();
				$this->load->view('admin_dashboard/email/enviados',$data);
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function nuevo(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$this->load->view('admin_dashboard/email/form');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function borrados(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$this->load->view('admin_dashboard/email/form');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function sent(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_admin =  $user->id_user;
				$My_email =$this->uri->segment(3);
				 $data['email'] = $this->processes_model->sent($My_email,$My_admin);
				$this->load->view('admin_dashboard/email/sent',$data);
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function EXCHANGE(){	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$data['Usuarios'] = $this->Lon_In_model->perfil($My_Id);
				$data['dashboard'] = "EXCHANGE";
				$this->load->view('dashboard/vendor/head',$data);
				$this->load->view('dashboard/vendor/menu');
				$this->load->view('dashboard/vendor/menu2');
				$this->load->view('dashboard/EXCHANGE');
				$this->load->view('dashboard/vendor/script');

			}elseif ($this->session->userdata('rol') == '2') {
				echo "usted no tiene los permisos";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
}
