<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class processes extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('processes_model');
		$this->load->model('Lon_In_model');
	}
	public function index()
	{
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$this->load->view('vendor/head');
				$this->load->view('welcome_message');
				$this->load->view('vendor/script');			
			}elseif ($this->session->userdata('rol') == '2') {
				$this->load->view('vendor/head');
				$this->load->view('welcome_message');
				$this->load->view('vendor/script');
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
	public function Proceso_data(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$proceso_data = $_POST['opcion'];
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;

				if ($proceso_data = 1) {
					$this->processes_model->Process_daily($My_Id);
				}elseif ($proceso_data = 2) {
					$this->processes_model->Process($My_Id);
				}else{
					echo "Porfavor no toque el codigo";
				}			
			}elseif ($this->session->userdata('rol') == '2') {
				$proceso_data = $_POST['opcion'];
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;

				if ($proceso_data = 1) {
					$this->processes_model->Process_daily($My_Id);
				}elseif ($proceso_data = 2) {
					$this->processes_model->Process($My_Id);
				}else{
					echo "Porfavor no toque el codigo";
				}
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
	public function table(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');
				$user = $this->Lon_In_model->datos($usuario);
				$My_Id = 1;
				$tables_process = $this->processes_model->tables_process($My_Id);
				foreach ($tables_process as $pross) {
					echo "<tr>
						<td>".$pross['nombre']."</td>
						<td>$".$pross['id_cuenta_de']."</td>
						<td>".$pross['id_cuenta_a']."</td>
						<td>".$pross['monto']."</td>
						<td>".$pross['fecha']."</td>
						<td>".$pross['hora']."</td>
					  </tr>";
				}			
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');
				$user = $this->Lon_In_model->datos($usuario);
				$My_Id = 1;
				$tables_process = $this->processes_model->tables_process($My_Id);
				foreach ($tables_process as $pross) {
					echo "<tr>
						<td>".$pross['nombre']."</td>
						<td>$".$pross['id_cuenta_de']."</td>
						<td>".$pross['id_cuenta_a']."</td>
						<td>".$pross['monto']."</td>
						<td>".$pross['fecha']."</td>
						<td>".$pross['hora']."</td>
					  </tr>";
				}
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
	public function accounts(){
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');
				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$tables_process = $this->processes_model->tables_accounts($My_Id);
				foreach ($tables_process as $pross) {
					echo "<tr>";
						//<td>".$pross['c_encargado_cuenta']."</td>
					echo "<td>$".$pross['c_monto_inicial']."</td>
						<td>$".$pross['monto_actual']."</td>
						<td>$".$pross['c_monto_final']."</td>
						<td>".$pross['ct_tipo_cuenta']."</td>
						<td>".$pross['porcentaje']."%</td>
						<th><button class='btn btn-success'>remove</button></th>
					  </tr>";
				}			
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');
				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$tables_process = $this->processes_model->user_cuentas();
				foreach ($tables_process as $pross) {
					echo "<tr>
						<td>".$pross['nombre']."</td>
						<td>".$pross['codigo_cienta']."</td>
						<td>$".$pross['monto']."</td>
						<td>".$pross['type_cuenta'].' '.$pross['porcentaje']."%</td>
						<td></td>
						<td></td>
						<th><button class='btn btn-success'>activo</button></th>
					  </tr>";
				}
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
//-------------------------------------admin------------------------------------------------
	public function users()
	{	
		if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				echo "usted no tiene los permisos";
			}elseif ($this->session->userdata('rol') == '2') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$user_reguistro = $this->processes_model->user_reguistro();
				foreach ($user_reguistro as $users) {
					echo "<a><tr>
                        	<td>".$users['nombre']."</td>
                        	<td>".$users['apellidos']."</td>
                        	<td>".$users['correo']."</td>
                           	<td>".$users['identificacion']."</td>
                        	<td>".$users['teelefono']."</td>
                        	<td>".$users['pais']."</td>
                        	<td>".$users['ciudad']."</td>
                        	<td>".$users['codigo_cienta']."</td>
                        	<td><a href=".base_url('profile/index').'/'.sha1($users['id_user'])." class'btn btn-primary'>Profile</a></td>
                      	 </tr>";
					}				
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}	
	}
}