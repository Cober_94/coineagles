<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log_in extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('Lon_In_model');
	}
	public function start(){
		$mail = $this->input->post('mail');
		$pass = $this->input->post('password');
		$validado = $this->Lon_In_model->validar($mail,$pass);
			if ($validado != 'fail') {
				$this->session->set_userdata('mail',$mail);
				$this->session->set_userdata('rol',$validado);

				if ($validado == '1') {
					redirect('coineagle/Home');
				}elseif ($validado == '2') {
					redirect('coineagle/Home');
				}
		}else{
			$this->session->set_flashdata('error', 'usuario o contraseña invalidos');
			redirect('coineagle');
		}
	}

	public function cerrar_sesion(){
			$this->session->sess_destroy();
			redirect('coineagle');
	}
	public function estado_cuenta(){
	if ($this->session->userdata('mail') !=NULL|| $this->session->userdata('mail')!='') {

			if ($this->session->userdata('rol') == '1') {
				$usuario = $this->session->userdata('mail');

				$user = $this->Lon_In_model->datos($usuario);
				$My_Id =  $user->id_user;
				$pin = $_POST['pin'];
				$My_cuenta = $this->Lon_In_model->validar_cuenta($My_Id);
				if ($My_cuenta->estado == 0) {
					if ($pin == $My_cuenta->pin) {
					echo $this->Lon_In_model->update_estado($My_Id);
					}else{
						echo 3;
					}
				}else{
					redirect('coineagle/Home');
				}
				
			}elseif ($this->session->userdata('rol') == '2') {
				echo "hola admin";
			}else{
				redirect('coineagle');
			}
		}else{
			redirect('coineagle');
		}
	}
}
