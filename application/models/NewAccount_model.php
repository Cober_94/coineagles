<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewAccount_model extends CI_Model {
	
	public function Save_User($data){
		if ($this->db->insert('user',$data)) {
				echo "1";
		}else{
				echo "Fail";
		}  
	}
	public function Save_cuenta($data){
		if ($this->db->insert('cuenta',$data)) {
				echo "1";
		}else{
				echo "Fail";
		}  
	}
	public function calculadora_BTC($data_btc,$moneda_b){
			if ($data_btc == NULL) {
				$btc_s = 0;
			}else{
				$centavos_b = 100;

				$satochis_b = 10000000000;

				$total_centavos_b = $moneda_b * $centavos_b;

				$tc_b = $total_centavos_b;
				$igual_b = $satochis_b / $tc_b;
				$i_b = $igual_b;

				$d_b = $i_b * $data_btc;
				$btc_s = round($d_b,0);
			}

		return $btc_s;
	}
	public function calculadora_LTH($data_lth,$moneda_l){
		if ($data_lth == NULL) {
			$btc_l = 0;
		}else{
			$centavos_l = 100;

			$satochis_l = 10000000000;

			$total_centavos_l = $moneda_l * $centavos_l;

			$tc_l = $total_centavos_l;
			$igual_l = $satochis_l / $tc_l;
			$i_l = $igual_l;

			$d_l = $i_l * $data_lth;
			$btc_l = round($d_l,0);
		}

		return $btc_l;
	}
	public function calculadora_ETH($data_eth,$moneda_e){
		
		if ($data_eth == NULL) {
			$btc_e = 0;
		}else{
			$centavos_e = 100;

			$satochis_e = 10000000000;

			$total_centavos_e = $moneda_e * $centavos_e;

			$tc_r = $total_centavos_e;
			$igual_e = $satochis_e / $tc_r;
			$i_e = $igual_e;

			$d_e = $i_e * $data_eth;
			$btc_e = round($d_e,0);
		}
		return $btc_e;
	}
	public function porcentage_BTC($BITCOIN){
		
			if ($BITCOIN >= 10.00 and $BITCOIN <= 2000.00){
			 	$porceng_btc = 5;
			 }elseif($BITCOIN >= 2000.01 and $BITCOIN <= 15000.00 ){
			 	$porceng_btc = 7;
			 }elseif($BITCOIN >= 15000.01){
			 	$porceng_btc = 9;
			 }else{
			 	$porceng_btc = "error";
			 }

		return $porceng_btc;
	}
	public function porcentage_LTH($LITECOIN){
		
			if ($LITECOIN >= 10.00 and $LITECOIN <= 2000.00){
			 	$porceng_ltc = 5;
			 }elseif($LITECOIN >= 2000.01 and $LITECOIN <= 15000.00 ){
			 	$porceng_ltc = 7;
			 }elseif($LITECOIN >= 15000.01){
			 	$porceng_ltc = 9;
			 }else{
			 	$porceng_ltc = "error";
			 }

		return $porceng_ltc;
	}
	public function porcentage_ETH($ETHEREUM){		
			
			 if ($ETHEREUM >= 10.00 and $ETHEREUM <= 2000.00){
			 	$porceng_eth = 5;
			 }elseif($ETHEREUM >= 2000.01 and $ETHEREUM <= 15000.00 ){
			 	$porceng_eth = 7;
			 }elseif($ETHEREUM >= 15000.01){
			 	$porceng_eth = 9;
			 }else{
			 	$porceng_eth = "error";
			 }

		return $porceng_eth;
	}
}