<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class processes_model extends CI_Model {

	public function Process($My_Id){
		$query = $this->db->query("SELECT * FROM transaccion WHERE id_user ='".$My_Id."' GROUP by mes");
		return $query->result_array();
	}		
	public function Process_daily($My_Id){
		$query = $this->db->query("SELECT * FROM transaccion WHERE id_user ='".$My_Id."' ORDER BY t_id_transaccion  DESC");
		return $query->result_array();
	}
	public function tables_accounts($My_Id){
		$query = $this->db->query("SELECT * FROM cuenta c INNER JOIN tipo_cuenta tc ON c.ct_id_tipo_cuenta = tc.ct_id_tipo_cuenta WHERE c.id_user = '".$My_Id."' AND c.c_estado_cuenta = 1");
		return $query->result_array();	
	}//---------------------------------------------------admin--------------------------------------
	public function user_reguistro(){
		$query = $this->db->query("SELECT c.*,u.* FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user where rol = 1 ORDER BY id_cuenta  DESC ");
		return $query->result_array();
	}
	public function user_cuentas(){
		$query = $this->db->query("SELECT * FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user INNER JOIN tb_type_cuneta tc ON c.tipo_cuenta = tc.tb_type WHERE u.rol = 1  ORDER BY id_cuenta DESC");
		return $query->result_array();
	}
	public function transaccion_de($My_Id){
		$query = $this->db->query("SELECT COUNT(t.id_transaccion) de FROM cuenta c INNER JOIN transaccion t  ON c.id_cuenta = t.id_cuenta_de WHERE id_user ='".$My_Id."' ORDER BY id_cuenta  DESC");
		return $query->row();
	}
	public function transaccion_a($My_Id){
		$query = $this->db->query("SELECT COUNT(t.id_transaccion) a FROM cuenta c INNER JOIN transaccion t  ON c.id_cuenta = t.id_cuenta_a WHERE id_user ='".$My_Id."' ORDER BY id_cuenta  DESC");
		return $query->row();
	}
	public function get_transaccion($My_Id){
		$query = $this->db->query("SELECT * FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user INNER JOIN transaccion t ON c.id_cuenta = t.id_cuenta_a WHERE c.id_user'".$My_Id."'");
		return $query->result_array();
	}
	public function tables_process_de($My_Id){
		$query = $this->db->query("SELECT * FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user INNER JOIN transaccion t ON c.id_cuenta = t.id_cuenta_de WHERE c.id_cuenta = '".$My_Id."'");
		return $query->result_array();	
	}
	public function tables_process_a($My_Id){
		$query = $this->db->query("SELECT * FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user INNER JOIN transaccion t ON c.id_cuenta = t.id_cuenta_a WHERE c.id_cuenta = '".$My_Id."'");
		return $query->result_array();	
	}
	public function tables_transacciones(){
		$query = $this->db->query("SELECT * FROM user u INNER JOIN cuenta c ON u.id_user = c.id_user INNER JOIN transaccion t ON c.id_cuenta = t.id_cuenta_de");
		return $query->result_array();	
	}
	public function email(){
		$query = $this->db->query("SELECT * FROM tb_emails ORDER BY id_emails  DESC");
		return $query->result_array();	
	}
	public function sent($My_email,$My_admin){
		$query = $this->db->query("SELECT * FROM tb_emails WHERE id_emails = '".$My_email."'AND id_user ='".$My_admin."'");
		return $query->row();	
	}
	public function btc($My_Id){
		$query = $this->db->query("SELECT * FROM btc where id_user ='".$My_Id."'");
		return $query->row();
	}
	public function ltc($My_Id){
		$query = $this->db->query("SELECT * FROM ltc where id_user ='".$My_Id."'");
		return $query->row();
	}
	public function eth($My_Id){
		$query = $this->db->query("SELECT * FROM eth where id_user ='".$My_Id."'");
		return $query->row();
	}
}

