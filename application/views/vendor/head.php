<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta charec set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- Page Title -->
        <title>CoinEagle</title>		
		<!-- Meta Description -->
        <meta name="description" content="Blue One Page CoinEagle">
        <meta name="keywords" content="one page, single page, onepage, responsive, parallax, CoinEagle">
        <meta name="author" content="Samuel Alvarado">
		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo base_url();?>img/icon1.png" rel="icon" >

		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
		<!-- Twitter Bootstrap css -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
		<!-- jquery.fancybox  -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fancybox.css">
		<!-- animate -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/main.css">
		<!-- media-queries -->
        <link rel="stylesheet" href="<?php echo base_url();?>css/media-queries.css">
		<!--para precios-->
		<link rel="stylesheet" href="<?php echo base_url();?>css/precios.css">
		<!--para buttons-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/buttons.css">
		<!--para alertas-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/notice.css">

		<link rel="stylesheet" href="<?php echo base_url();?>css/sweetalert2.css">
		
		<script src="<?php echo base_url();?>js/sweetalert2.min.js" ></script>	
        <script src="<?php echo base_url();?>view_front/js/modernizr-2.6.2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    </head>