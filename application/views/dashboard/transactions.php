  <?php 
if ($Usuarios->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $Usuarios->foto;
}
 ?>
    <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col-md-7">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Pagina</h6>
                  <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" style="width:107px;" alt="...">
                </div>
                
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
                <div class="row">
                  
                  <div class="col-md-12">
                    <h3> <?php if($this->session->flashdata('error')){?></h3>
                  <div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
                  <?php }elseif($this->session->flashdata('success')){?>
                    <div class="alert alert-success"><?= $this->session->flashdata('success')?></div>
                  <?php }?>
                    <form method="post" id="form" action="<?= base_url();?>transaccion/transaccion" >

                      <span>Choose system</span>
                      <select class="form-control" id="select" name="system">
                        <option value="BITCOIN">BITCOIN</option>
                        <option value="LITECOIN">LITECOIN</option>
                        <option value="ETHEREUM">ETHEREUM</option>
                      </select>
                      <br>
                      <span>Templates</span>
                      <select class="form-control" name="templates">
                        <option value="1">CHOOSE TEMPLATE...</option>
                      </select>
                      <br>
                      <span id="address">BITCOIN</span>-address
                      <input type="text" name="address" placeholder="13C3fxYMZzbt9HsTvCni779gqXyPadGtTQ" class="form-control">
                      <br>
                        <div class="col-md-6">
                        <span>Amount</span>
                        <input type="text" name="Amount" class="form-control" value="" id="Amount" placeholder="0">
                        </div>
                        <!--<div class="col-md-6">
                        <span>Amount in dollar</span>
                        <input type="text" name="Amount" class="form-control" value="" id="Amount" placeholder="00.00">
                        </div>-->
                        <div class="col-md-6">
                        <span>total</span>
                        <input type="text" name="total" class="form-control" value="0" id="total">
                        </div>
                        <br>
                        <span>Comission</span><span style="text-align: right; color: #ec7063;">    0% + 0.001</span>
                        <br>
                        <button class="btn btn-primary">
                        Send <span id=""></span>
                        </button>
                    </form>

                    <br>
                    <form method="post" action="https://payeer.com/merchant/">
                      <input type="hidden" name="m_shop" value="799763681">
                      <input type="hidden" name="m_orderid" value="1">
                      <input type="hidden" name="m_amount" value="1.00">
                      <input type="hidden" name="m_curr" value="USD">
                      <input type="hidden" name="m_desc" value="dGVzdA==">
                      <input type="hidden" name="m_sign"
                      value="9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F
                      00A08">
                      <!--
                      <input type="hidden" name="form[ps]" value="2609">
                      <input type="hidden" name="form[curr[2609]]" value="USD">
                      -->
                      <!--
                      <input type="hidden" name="m_params" value="">
                      -->
                      <input type="submit" name="m_process" value="send" />
                      </form>                    
                  </div>
                </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Core -->
  <script type="text/javascript">
      $("#Amount").keyup(function(){
        $.ajax({
            url : '<?php echo base_url()?>transaccion/Amount',
            type : 'post',
            data : $("form").serialize(),
        success: function(data) {
             //console.log(resultado);
             if (data == 1) {
              $("#Amount").css('border-color','green');
             }else{
              $("#Amount").css('border-color','red');
             }
             $('#total').val('');
             var Amount = $("#Amount");
             $('#total').val(Amount);

           }
        });
      });
        $("#Amount").blur(function(){
        $.ajax({
            url : '<?php echo base_url()?>transaccion/Amount',
            type : 'post',
            data : $("form").serialize(),
        success: function(data) {
             //console.log(resultado);
             if (data == 1) {
              $("#total").css('border-color','red');
             }else{
              $("#Amount").css('border-color','green');
             }
           }
        });
      });
       $("#select").change(function(){
        $.ajax({
            url : '<?php echo base_url()?>transaccion/system',
            type : 'post',
            data : $("form").serialize(),
        success: function(data) {
             //console.log(resultado);
              $("#address").html(data);
           }
        });
      });   
  </script>
 