  <?php 
if ($Usuarios->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $Usuarios->foto;
}
 ?>
    <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col-md-7">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Pagina</h6>
                  <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" style="width:107px;" alt="...">
                </div>
                
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
                <div class="row">
                  
                  <div class="col-md-12">
                    <h3> <?php if($this->session->flashdata('error')){?></h3>
                  <div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
                  <?php }elseif($this->session->flashdata('success')){?>
                    <div class="alert alert-success"><?= $this->session->flashdata('success')?></div>
                  <?php }?>
                      
                      <form id="form">
                        <p>Select account</p>
                        <select name="system" id="select">
                          <option value="BITCOIN">BITCOIN</option>
                          <option value="LITECOIN">LITECOIN</option>
                          <option value="ETHEREUM">ETHEREUM</option>
                        </select>
                        <p>Select account</p>
                        <select name="account_tow" id="account_tow">
                          <option value="LITECOIN">LITECOIN</option>
                          <option value="ETHEREUM">ETHEREUM</option>
                          <option value="BITCOIN">BITCOIN</option>
                        </select>
                        <p>Amount of the <span id="address">BITCOIN</span> account</p>
                        <input type="text" onkeyup="account()" class="form-control" name="Amount" id="Amount">
                        <p>conversion to <span id="address_two">BITCOIN</span></p>
                        <p class="two" id="two">00.00</p>
                        <p>conversion to <span id="conversion">LITECOIN</span></p>
                        <p class="to">00.00</p>
                      </form>
                  </div>
                </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function() {
        console.log("el documento está preparado");
    });
    function account(){
      var url = '<?php echo base_url();?>transaccion/Amount';

      $.ajax({
        type : 'post',
        data : $('#form').serialize(),
        url : url,
        success:function(data){
          if (data == 1){
             $("#Amount").css('border-color','blue');
             $("#two").css('color','blue');

          }else{
             $("#Amount").css('border-color','red');
             $("#two").css('color','red');
          }
          $(".to").css('color','blue');
        }
      });
    }
    $("#Amount").keyup(function(){
      $.ajax({
        type : 'post',
        data : $('#form').serialize(),
        url : '<?= base_url();?>transaccion/mont',
        success:function(data){
          $(".to").html(data);
        }
      });
    });
    $("#Amount").keyup(function(){
      $.ajax({
        type : 'post',
        data : $('#form').serialize(),
        url : '<?= base_url();?>transaccion/two',
        success:function(data){
          $(".two").html(data);
        }
      });
    });
    $("#select").change(function(){
        $.ajax({
            url : '<?php echo base_url()?>transaccion/system',
            type : 'post',
            data : $("form").serialize(),
        success: function(data) {
             //console.log(resultado);
              $("#address").html(data);
              $("#address_two").html(data);
           }
        });
      });
     $("#account_tow").change(function(){
        $.ajax({
            url : '<?php echo base_url()?>transaccion/account_tow',
            type : 'post',
            data : $("form").serialize(),
        success: function(data) {
             //console.log(resultado);
              $("#conversion").html(data);
           }
        });
      });
  </script>