<body id="body">
    <?php 
if ($Usuarios->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $Usuarios->foto;
}
 ?>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="./index.html">
        <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" class="navbar-brand-img" alt="...">
      </a>
      <!--================Start User Responsive============= -->
      <ul class="nav align-items-center d-md-none">
        <!--===inc user profile====-->
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="<?php echo base_url();?>assets/img/profile/<?=$Photo?>">
              </span>
            </div>
          </a>
          <!-- user dropdown start-->
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="" class="dropdown-item">
              <i class="fa fa-user"></i>
              <span>My profile</span>
            </a>         
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="fa fa-user"></i>
              <span>Logout</span>
            </a>
          </div>
           <!-- user dropdown end-->
        </li>
        <!--===inc user profile====-->
      </ul>
      <!--================End User Responsive============= -->
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="./index.html">
                <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>
        <!-- start Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>coineagle/Home">
              <i class="fa fa-home text-primary"></i> Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>coineagle/transactions">
              <i class="fa fa-money text-blue"></i> Transactions
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>coineagle/accounts">
              <i class="fa fa-credit-card text-blue"></i> History
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>coineagle/EXCHANGE">
              <i class="  fa fa-handshake-o text-blue"></i> EXCHANGE
            </a>
          </li> 
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>coineagle/transactions">
              <i class="  fa fa-handshake-o text-blue"></i> community
            </a>
          </li>          
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->
        <h6 class="navbar-heading text-muted">Ajustes</h6>
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">

          <li class="nav-item">
            <a class="nav-link" href="">
              <i class="fa fa-cogs"></i> Ajustes
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>