 <body>
    <!-- preloader -->
		<div id="preloader" style="text-align: center;">
			<img src="<?php echo base_url();?>img/pre-inicio.gif"  class="col-lg-3 col-md-3 col-sm-6">
		</div>
		<!-- end preloader -->
		<h3> <?php if($this->session->flashdata('error')){?></h3>
		<div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
		<?php }?>
        <!-- ======================== Navigation ==================================== -->
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">
							<img src="<?php echo base_url();?>img/logo_todo.png" alt="Brandi">
						</h1>
					</a>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="<?php echo base_url()?>#body">Home</a></li>
                        <li><a href="<?php echo base_url()?>#About">About Us</a></li>
                        <li><a href="<?php echo base_url()?>#Investment">Investment</a></li>
                        <li><a href="<?php echo base_url()?>#facts">Coin Eagle</a></li>
                        <li><a href="<?php echo base_url()?>#faq">FAQ</a></li>
                        <li><a href="<?php echo base_url()?>#contact">Support</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
            </div>
        </header>
        <!--================================== Home Slider==================================== -->
        <section id="slider">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    
                    <!-- single slide -->
                    <div class="item active" style="background-image: url(<?php echo base_url();?>img/baner4.jpg);">
                        <div class="carousel-caption">
                            <h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated">Bienvenido a<span> CoinEagle</span>!</h2>
                            <br>
                            <p data-wow-duration="1000ms" class="wow slideInRight animated">Debes verificar tu cuenta primero</p>
                            <br>
                            <form id="form">
                                <input type="text" class="form-control" name="pin" id="pin" placeholder="escribe en pin de seguridad que enviamos a tu correo">
                                <br>
                                
                            </form>
                            <button onclick="validar()" class="btn btn-primary">validar</button>
                                <br>
                                <a href="">Reenviar pin de verificacion</a>
                        </div>
                    </div>
                    <!-- end single slide -->
                </div>
                <!-- End Wrapper for slides -->
                
            </div>
        </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script type="text/javascript">
            $(document).ready(function (){
                    $('#pin').mask('0000-000000-000-0');
                       
                    });
            function validar(){
                    var url = '<?php echo base_url();?>log_in/estado_cuenta';
                         $.ajax({
                                url : url,
                                type : 'Post',
                                data : $('#form').serialize(),
                                success:function(data){
                                   if(data==1){
                                    
                                            window.location='<?php echo base_url()?>coineagle/Home'; 
                                    }else{
                                    alert(data);
                                    $('.form-control').val('');
                                  }
                              }
                      });               
                } 
        </script>