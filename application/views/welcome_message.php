 <body id="body">
 	
	
		<!-- preloader -->
		<div id="preloader" style="text-align: center;">
			<img src="<?php echo base_url();?>img/pre-inicio.gif"  class="col-lg-3 col-md-3 col-sm-6">
		</div>
		<!-- end preloader -->
		
        <!-- ======================== Navigation ==================================== -->
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">
							<img src="<?php echo base_url();?>img/logo_todo.png" alt="Brandi">
						</h1>
					</a>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="#body">Home</a></li>
                        <li><a href="#About">About Us</a></li>
                        <li><a href="#Investment">Investment</a></li>
                        <li><a href="#facts">Coin Eagle</a></li>
                        <li><a href="#faq">FAQ</a></li>
                        <li><a href="#contact">Support</a></li>
                        <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#loginModal">Log in </a></li>
                        <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#singModal">New Account</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
            </div>
        </header>
        <!--=========================== End  Navigation ==================================== -->
			
        <!--================================== Home Slider==================================== -->
		<section id="slider">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			
				<!-- Indicators bullet -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
				<!-- End Indicators bullet -->				
				
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					
					<!-- single slide -->
					<div class="item active" style="background-image: url(<?php echo base_url();?>img/baner4.jpg);">
						<div class="carousel-caption">
							<h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated">Welcome to<span></span></h2>
							<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/Coin Eagle!!</span> Site Bitcoin.</h3>
							<p data-wow-duration="1000ms" class="wow slideInRight animated">Where you can earn a lot!.</p>
							
							<ul class="social-links text-center">
								<li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
							</ul>
						</div>
					</div>
					<!-- end single slide -->
					
					<!-- single slide -->
					<div class="item" style="background-image: url(<?php echo base_url();?>img/baner2.jpg);">
						<div class="carousel-caption">
							<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated">Coin<span> Eagle</span>!</h2>
							<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/Blockchain</span> Site.</h3>
							<p data-wow-duration="1000ms" class="wow slideInRight animated">The future is now with bitcoins</p>
							
							<ul class="social-links text-center">
								<li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
							</ul>
						</div>
					</div>

					<div class="item" style="background-image: url(<?php echo base_url();?>img/baner3.jpg);">
						<div class="carousel-caption">
							<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated">Coin<span> Eagle</span>!</h2>
							<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/The Best</span> Moment.</h3>
							<p data-wow-duration="1000ms" class="wow slideInRight animated">To start investing is NOW</p>
							
							<ul class="social-links text-center">
								<li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
							</ul>
						</div>
					</div>
					<!-- end single slide -->
					
				</div>
				<!-- End Wrapper for slides -->
				
			</div>
		</section>
        <!--========================== End Home SliderEnd ==================================== -->
		
        <!--===================================== Features ==================================== -->		
		<section id="About" class="features">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
						<h2>A Little More</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<!-- service item -->
					<div class="col-md-6 wow fadeInLeft" data-wow-duration="500ms">
						<div class="service-item">
							<div class="col-lg-12 col-md12- col-sm-6">
								<img src="<?php echo base_url();?>img/somos.png" width="100%">
							</div>
						</div>
					</div>
					<!-- end service item -->
					
					<!-- service item -->
					<div class="col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-users fa-2x"></i>
							</div>
							
							<div class="service-desc">
								<h3>We Are</h3>
								<br>
								<h3><span class="color">coineagle.info</span></h3><br>
								<p>
									<h4 class="sec-title" style="text-align: justify;">.info brings together a community of traders and qualified investors dedicating to creating a beneficial and profitable environment for our members. Now you can turn your money-making dreams into a reality!
										<br><br><br>
										so if You want to begin investing, but you do not know how? coineagle.info will assist you to fulfill your investment potential. We provide an encouraging collaboration that will cause individual investor the income from the company's trading activities. The most genuine transactions and supreme earnings from the allocation of funds are supported by the minimum level of ranches and financial contingencies.
										<br><br><br>
										<div style="text-align: right;">
											<img src="<?php echo base_url();?>img/logo_we_are.png" >
										</div>
										
									</h4>
								</p>
								<div style="text-align: right;">
									<br><br>
									<a href="<?php echo base_url();?>about" class="btns btn-4">View More <i class="fa fa-arrow-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<!-- end service item -->

				</div>
			</div>
		</section>		
        <!--==================================== End Features==================================== -->
		
        <!--===========================================Investment==================================== -->		
		<section id="Investment" class="team">
			<div class="container">
				<div class="row">
		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>Investment</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					
					<div class="sec-sub-title text-center wow fadeInRight animated" data-wow-duration="500ms">
						<p>The account types that you can create and access<z/p>
					</div>
	
					<section>
						  <div class="container-fluid">
						    <div class="container">
						      <div class="row">

								 <!-- END Col two -->
						        <div class="col-sm-4">
						          <div class="cards text-center">
						            <div class="title">
						              <i class="" aria-hidden="true"><img src="<?php echo base_url();?>img/moneda_bronce.png"></i>
						              <h2>Bronze</h2>
						            </div>
						            <div class="price">
						              <h4>5<sup>%</sup></h4>
						            </div>
						            <div class="option">
						              <ul>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> min amount: $10</li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> max amount: $2000 </li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> Contract: 35 days </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Capital Incluided </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Percent instant accrued daily and available to withdraw </li>
						              </ul>
						            </div>
						            <a href="#">Sing in Now </a>
						          </div>
						        </div>
						        <!-- END Col three -->

						        <div class="col-sm-4">
						          <div class="cards text-center">
						            <div class="title">
						              <i class="" aria-hidden="true"><img src="<?php echo base_url();?>img/moneda_plata.png"></i>
						              <h2>Silver</h2>
						            </div>
						            <div class="price">
						              <h4>7<sup>%</sup></h4>
						            </div>
						            <div class="option">
						              <ul>
						              	<li> <i class="fa fa-arrow-right" aria-hidden="true"></i> min amount: $2001 </li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> max amount: $15000 </li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> Contract: 35 days </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Capital Incluided </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Percent instant accrued daily and available to withdraw </li>
						              
						              </ul>
						            </div>
						            <a href="#">Sing in now </a>
						          </div>
						        </div>
						        <!-- END Col one -->

						        <div class="col-sm-4">
						          <div class="cards text-center">
						            <div class="title">
						              <i class="" aria-hidden="true"><img src="<?php echo base_url();?>img/moneda_oro.png"></i>
						              <h2>Gold</h2>
						            </div>
						            <div class="price">
						              <h4>9<sup>%</sup></h4>
						            </div>
						            <div class="option">
						              <ul>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> min amount: $15001 </li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> max amount: $50000 </li>
						              <li> <i class="fa fa-arrow-right" aria-hidden="true"></i> Contract: 35 days </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Capital Incluided </li>
						              <li> <i class="fa fa-check" aria-hidden="true"></i> Percent instant accrued daily and available to withdraw </li>
						              </ul>
						            </div>
						            <a href="#">Sing in Now </a>
						          </div>
						        </div>

						      </div>
						    </div>
						  </div>
						</section>
						<br>
						<br>
						<br>
						<div style="text-align: right;">
							<a href="investment.html" class="btns btn-3">View More <i class="fa fa-arrow-right"></i></a>
						</div>
				</div>
			</div>
		</section>
        <!--======================================End Investment ==================================== -->
		
		<!--========================================Graphs of clients==================================== -->		
		<section id="facts" class="facts">
			<div class="parallax-overlay">
				<div class="container">
					<div class="row number-counters">
						
						<div class="sec-title text-center mb50 wow rubberBand animated" data-wow-duration="1000ms">
							<h2>Graphs of clients, countries in what is Coin Eagle</h2>
							<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
						</div>
						
						<!-- first count item -->
						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
							<div class="counters-item">
								<i class="fa fa-clock-o fa-3x"></i>
								<strong data-to="365">0</strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Service days of the year</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
							<div class="counters-item">
								<i class="fa fa-users fa-3x"></i>
								<strong data-to="120">0</strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Satisfied Clients</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
							<div class="counters-item">
								<i class="fa fa-flag fa-3x"></i>
								<strong data-to="60">0</strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p> Countries </p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
							<div class="counters-item">
								<i class="fa fa-check fa-3x"></i>
								<strong data-to="100">0</strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Recommendations</p>
							</div>
						</div>
						<!-- end first count item -->
				
					</div>
				</div>
			</div>
		</section>	
        <!--===================== Graphs of clients==================================== -->
		
		<!--================================ FAQ==================================== -->		
		<section id="faq" class="features">
			<div class="container">
				<div class="row mb50">
				
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>Frequent Questions</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					
					<div class="sec-sub-title text-center wow rubberBand animated" data-wow-duration="1000ms">
						<p>In this section the questions of the people on how to start in Coin Eagle are solved</p>
					</div>

					<div class="service-desc">
						<div class="row">
							<div class="notice notice-danger">
        						<strong>How to begin with coineagle.info?</strong>
        						<p>In order to begin working with our firm and be able to get live and/or passive income, each user is obligated to complete the registration process, which will ultimately create your account.</p>
    						</div>
    						<div class="notice notice-danger">
        						<strong>How reliable is your coineagle.info?</strong>
        						<p>The business plan we designed, and our skilled team, has shown an immeasurable result in the coineagle.info's lifetime. The conclusion is the majority of successfully realized trading transactions. coineagle.info has always been linked with reliability for us and our allies.</p>
    						</div>
    						<div class="notice notice-danger">
        						<strong>Who is qualified to become an investor/member?</strong>
        						<p>In view of the legislative part, since the coineagle is officially registered, it's possible for anyone who has reached the age of 18 years or more to obtain a profit from the investment and (or) from the partnership fee.</p>
    						</div>
						</div>

						<div style="text-align: right;">
								<br>
							<a href="questions.html" class="btns btn-2">View More <i class="fa fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</section>		
        <!--=========================== End FAQ  ==================================== -->

		<!--=========================== Contact Us  ==================================== -->
        <section id="contact" class="contact">
			<div class="container">
				<div class="row mb50">
				
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>Contact us</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					
					<div class="sec-sub-title text-center wow rubberBand animated" data-wow-duration="1000ms">
						<p>In this section the questions of the people on how to start in Coin Eagle are solved</p>
					</div>
					
				</div>
					
					<!-- contact address -->
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInLeft animated" data-wow-duration="500ms">
						<div class="contact-address">
							<h3>Contact forms</h3>
							<p>Email: info@coineagle.com</p>
							<p>Tel: 2222-2223</p>
						</div>
					</div>
					<!-- end contact address -->
					
					<!-- contact form -->
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="contact-form">
							<h3>Hello!, you can contact us through the form.</h3>
							<form action="<?= base_url();?>log_in/start" method="post" id="contact-form" name="contact">
								<div class="input-group name-email">
									<div class="input-field">
										<input type="text" name="name" id="name" placeholder="Name" class="form-control">
									</div>
									<div class="input-field">
										<input type="email" name="email" id="email" placeholder="Email" class="form-control">
									</div>
								</div>
								<div class="input-group">
									<textarea name="message" id="message" placeholder="Message" class="form-control"></textarea>
								</div>
								<div class="input-group">
									<input type="submit" id="form-submit" class="pull-right" value="Send message">
								</div>
							</form>
							<br>
						</div>
					</div>
					<!-- end contact form -->
					
					<!-- footer social links -->
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<ul class="footer-social">
							<li><a href=""><i class="fa fa-behance fa-2x"></i></a></li>
							<li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
							<li><a href=""><i class="fa fa-dribbble fa-2x"></i></a></li>
							<li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
						</ul>
					</div>
					<!-- end footer social links -->
				</div>
		</section>
		<!--=========================== Contact Us  ==================================== -->

		<!--=========================== footer ==================================== -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
						<div class="footer-single">
							<img src="<?php echo base_url();?>img/footer-logo1.png" alt="">
							<p style="text-align: justify;">so if You want to begin investing, but you do not know how? coineagle.info will assist you to fulfill your investment potential. We provide an encouraging collaboration that will cause individual investor the income from the company's trading activities.</p>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="footer-single">
							<h6>Subscribe </h6>
							<form action="#" class="subscribe">
								<input type="text" placeholder="email" name="subscribe" id="subscribe">
								<input type="submit" value="&#8594;" id="subs">
							</form>
							<p>The most genuine transactions and supreme earnings from the allocation of funds are supported by the minimum level of ranches and financial contingencies.</p>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<div class="footer-single">
							<h6>Explore</h6>
							<ul>
								<li><a href="about.html">About Us</a></li>
								<li><a href="investment.html">Subscriptions</a></li>
								<li><a href="index.html">Home</a></li>
							</ul>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
						<div class="footer-single">
							<h6>Support</h6>
							<ul>
								<li><a href="support.html">Contact Us</a></li>
								<li><a href="support.html">send Message</a></li>
								<li><a href="questions.html">Frequent Questions</a></li>
							</ul>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © 2019 <a href="index.html">Coin Eagle</a>. All rights reserved.<a href=""></a>
						</p>
					</div>
				</div>
			</div>
		</footer>
		<!--=========================== modal login  ==================================== -->
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    	
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                        <h4 class="modal-title"><img src="<?php echo base_url();?>img/logo_we_are.png"></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="row">
                    		<h4 class="" style="text-align: center; color: #2d8fd0">Login with your username and password</h4>
                    		<img src="<?php echo base_url();?>">
                    		<form name="login" action="<?= base_url();?>log_in/start" method="post">

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
                    					<div class="service-icon">
											<i class="fa fa-user fa-2x"></i>
										</div>
                    				</div>
                    				<div class="col-lg-10 col-md-10 col-sm-10">
                    					<label>Mail:</label>
                    					<input type="text" name="mail" required="true" id="user_name" class="form-control">
                    				</div>
                    			</div>
                    			<br>
                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
                    					<div class="service-icon">
											<i class="fa fa-key fa-2x"></i>
										</div>
                    				</div>	
                    				<div class="col-lg-10 col-md-10 col-sm-10">
                    					<label>Password:</label>
                    					<input type="password" name="password" required="true" id="pass" class="form-control">
                    				</div>
                    			</div>
                    	</div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <input type="submit" value="Login" class="btn btn-primary">
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!--=========================== End Modal  ==================================== -->

        <!--=========================== modal new acount  ==================================== -->
		<div class="modal fade" id="singModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    	
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <h4 class="modal-title"><img src="<?php echo base_url();?>img/logo_we_are.png"></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="row">
                    		<h4 class="" style="text-align: center; color: #2d8fd0">Create new Account in Coin Eagle!</h4>
                    		<br>
                    		<h6 style="text-align: center; color: #2d8fd0">Fields containing an 'X' are required</h6>
                    		
                    		<form name="login" id="form">
                    			<br>
                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-user"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>First Name: <strong>x</strong></label>
	                    					<input type="text" name="user_name" required="true" id="user_name" class="form-control">
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-user"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Last Name: <strong>x</strong></label>
	                    					<input type="text" name="last_name" required="true" id="last_name" class="form-control">
	                    				</div>
	                    			</div>
                    			</div>

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<br>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-book"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Email: <strong>x</strong></label>
	                    					<input type="text" name="email" required="true" id="email" class="form-control">
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-phone"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Phone number: <strong>x</strong></label>
	                    					<input type="text" name="phone" id="phone" required="true" class="form-control">
	                    				</div>
                    				</div>
                    			</div>

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<br>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-phone"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Password : <strong>x</strong></label>
	                    					<input type="text" name="password" id="password" required="true" class="form-control">
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-credit-card"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Birthdate : </label>
	                    					<input type="date" name="Birthdate" id="Birthdate" class="form-control">
	                    				</div>
	                    			</div>
	                    			
                    			</div>
	                    			<br><br><br><br>                   			
                    			<div class="modal-footer">
                        			<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        			<button class="btn btn-primary" onclick="NewAccount()" data-dismiss="modal">Create Accoun</button>
                    			</div>
                    		</form>
                    	</div>              	
                    </div>
                </div>
            </div>
        </div>
        <!--=========================== end modal new acount  ==================================== -->
   		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
		<script type="text/javascript">
			function NewAccount(){
    		        var url = '<?php echo base_url();?>NewAccount/save';
				         $.ajax({
				                url : url,
								type : 'Post',
								data : $('#form').serialize(),
				                success:function(data){
				                   if(data==1) {
				                   	swal({
									  	text: "Excelente! ya eres parte de la familia coineagle",
									  	type: 'success',
									  	showCancelButton: true,
									  	confirmButtonText: 'Log in',
									  	
									},
									function(isConfirm) {
									  	if (isConfirm) {
									    	window.location='<?php echo base_url()?>coineagle/login'; 
									  	}
									});
				                  }else{
				                      alert(data);
				                     }
				              }
				      });         	    
				} 
		</script>