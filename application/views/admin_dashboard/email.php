  <?php 
if ($admin->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $admin->foto;
}
 ?>
    <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <br><br>
                </div>
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
              
                <!--menu-->
                <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
                <div class="container-fluid">
                 <ul class="navbar-nav">
                    <li class="nav-item ">
                      <button class="nav-link text-blue" onclick="nuevo()">
                        <i class="fa fa-plus text-blue"></i> Nuevo Correo
                      </button>
                    </li>
                    <li class="nav-item ">
                      <button class="nav-link text-blue" onclick="Correo()">
                        <i class="fa fa-user text-blue"></i> Enviados
                      </button>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link text-blue" href="">
                        <i class="fa fa-credit-card text-blue"></i> Borrados
                      </a>
                    </li>          
                  </ul>
                </div>
              </nav> 
              <!--end menu-->

              <div id="email"></div>
            </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank"><img src=""></a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Coin Eagle</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
<script type="text/javascript">
   $(document).ready( function () {
          Correo(); 
          $('#table').DataTable();
          
      });
      function Correo(){
        var url = '<?php echo base_url()?>coineagle/Enviados';
         $.ajax({
          url : url,
          type : 'post',
          success:function(data){
            $('#email').html('');
            $('#email').html(data);
          }
        });
      }
      function nuevo(){
        var url = '<?php echo base_url()?>coineagle/nuevo';
         $.ajax({
          url : url,
          type : 'post',
          success:function(data){
            $('#email').html('');
            $('#email').html(data);
          }
        });
      }
      function sent(dir)
      {
        $('#email').load(dir, function () {
          $('#content').html({show: true});
        });
      }
  </script>