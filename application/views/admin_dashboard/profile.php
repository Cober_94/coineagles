  <?php 
if ($Usuarios->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $Usuarios->foto;
}
 ?>
    <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Pagina</h6>
                  <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" style="width:107px;" alt="...">
                </div>
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
              <h5>Personal information of the user</h5>
                <div class="row">

                  <table class="table">
                    <thead>
                      <tr>
                        <th><img alt="Image placeholder" style="width: 100px; border-radius:14px;" src="<?php echo base_url();?>assets/img/profile/<?=$Photo?>"></th>
                        <th>
                           
                        </th>
                        <th><a href="" class="btn btn-success">Activo</a><a href="" class="btn btn-outline-success">procesos</a></th>
                      </tr>                     
                    </thead>
                    <tbody>
                      <tr>
                        <td>First Name</td>
                        <td><?=$Usuarios->nombre?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Last Name</td>
                        <td><?=$Usuarios->apellidos?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Mail</td>
                        <td><?=$Usuarios->correo?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>country</td>
                        <td><?=$Usuarios->pais?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>City</td>
                        <td><?=$Usuarios->ciudad?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>identification</td>
                        <td><?=$Usuarios->identificacion?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>phone number</td>
                        <td><?=$Usuarios->teelefono?></td>
                        <td></td>
                      </tr>
                      <tr> 
                        <td>se unio</td>
                        <td><?=$Usuarios->fecha?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>fecha de nacimiento</td>
                        <td><?=$Usuarios->fecha_nacimiento?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>monto de cuenta</td>
                        <td>$ <?=$Usuarios->monto?></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>tipo de cuenta</td>
                        <td><?=$Usuarios->type_cuenta?> <?=$Usuarios->porcentaje?>%</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>transacciones de</td>
                        <td><a href="<?= base_url()?>coineagle/transactions/<?=$My_Id?>/1" class="btn btn-outline-info"><?=$de->de?></a></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>transacciones a</td>
                        <td><a href="<?= base_url()?>coineagle/transactions/<?=$My_Id?>/2" class="btn btn-outline-info"><?=$a->a?></a></td>
                        <td></td>
                      </tr>

                    </tbody>
                  </table>
                </div>
            </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank"><img src=""></a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Coin Eagle</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
 