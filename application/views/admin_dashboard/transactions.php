  <?php 
if ($admin->foto == null) {
  $Photo = "coin_profile.jpg";
}else{
  $Photo = $admin->foto;
}
 ?>
    <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col-md-7">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Pagina</h6>
                  <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" style="width:107px;" alt="...">
                </div>
                
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
                <div class="row">

                    <table class="table" id="myTable">
                        <thead>
                          <tr>
                            <th>usuarios</th>
                            <th>de</th>
                            <th>a</th>
                            <th>monto</th>
                            <th>fecha</th>
                            <th>hora</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          foreach ($dea as $pross) {
                                echo "<tr>
                                  <td>".$pross['nombre']."</td>
                                  <td>".$pross['id_cuenta_de']."</td>
                                  <td>".$pross['id_cuenta_a']."</td>
                                  <td>$ ".$pross['monto']."</td>
                                  <td>".$pross['fecha']."</td>
                                  <td>".$pross['hora']."</td>
                                  </tr>";
                              }
                           ?>
                        </tbody>
                      </table>  
                </div>                
            </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank"><img src=""></a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Coin Eagle</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
<script type="text/javascript">
   $(document).ready( function () {
          process();
           $('#myTable').DataTable();
      });
      function process(){
        var url = '<?= base_url();?>processes/table';
         $.ajax({
          url : url,
          type : 'post',
          success:function(data){
            $('#process').html(data);
          }
        });
      }
  </script>
   <!--Argon Scripts -->
  <!-- Core -->
 