 <!-- Header -->
    <div class="header bg-gradient-property pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-12">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Pagina</h6>
                  <img src="<?php echo base_url();?>assets/img/brand/coin_logo.png" style="width:107px;" alt="...">
                </div>
              </div>
            </div>
            <!--start content-->
            <div class="card-body">
              <h5>users information</h5>
                <div class="row">
                  <table class="table">
                    <thead>
                      <tr> 
                        <th>first name</th>
                        <th>last name</th>
                        <th>mail</th>
                        <th>identification</th>
                        <th>phone number</th>
                        <th>country</th>
                        <th>city</th>
                        <th>account number</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="user">
                      
                    </tbody>
                  </table>
                </div>
            </div>
            <!--end content-->
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank"><img src=""></a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Coin Eagle</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script type="text/javascript">
   $(document).ready( function () {
          process();
          $('.table').DataTable();
           
      });
      function process(){
        var url = '<?= base_url();?>processes/users';
         $.ajax({
          url : url,
          type : 'post',
          success:function(data){
            $('#user').html(data);
          }
        });
      }
  </script>
 