 <body>
 	
	
		<!-- preloader -->
		<div id="preloader" style="text-align: center;">
			<img src="<?php echo base_url();?>img/pre-inicio.gif"  class="col-lg-3 col-md-3 col-sm-6">
		</div>
		<!-- end preloader -->
		<h3> <?php if($this->session->flashdata('error')){?></h3>
		<div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
		<?php }?>
        <!-- ======================== Navigation ==================================== -->
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">
							<img src="<?php echo base_url();?>img/logo_todo.png" alt="Brandi">
						</h1>
					</a>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="<?php echo base_url()?>#body">Home</a></li>
                        <li><a href="<?php echo base_url()?>#About">About Us</a></li>
                        <li><a href="<?php echo base_url()?>#Investment">Investment</a></li>
                        <li><a href="<?php echo base_url()?>#facts">Coin Eagle</a></li>
                        <li><a href="<?php echo base_url()?>#faq">FAQ</a></li>
                        <li><a href="<?php echo base_url()?>#contact">Support</a></li>
                        <li><a class="dropdown-item" href="<?php echo base_url()?>coineagle/login">Log in </a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
            </div>
        </header>
        <!--================================== Home Slider==================================== -->
        <section id="slider">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    
                    <!-- single slide -->
                    <div class="item active" style="background-image: url(<?php echo base_url();?>img/baner4.jpg);">
                        <div class="carousel-caption">
                            <div class="">
                              <form name="login" action="<?= base_url();?>log_in/start" method="post">

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="col-lg-2 col-md-2 col-sm-2"><br>
                                        <div class="service-icon">
                                            <i class="fa fa-user fa-2x"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10">
                                        <label>Mail:</label>
                                        <input type="text" name="mail" required="true" id="user_name" class="form-control">
                                    </div>
                                </div>
                                <br>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="col-lg-2 col-md-2 col-sm-2"><br>
                                        <div class="service-icon">
                                            <i class="fa fa-key fa-2x"></i>
                                        </div>
                                    </div>  
                                    <div class="col-lg-10 col-md-10 col-sm-10">
                                        <label>Password:</label>
                                        <input type="password" name="password" required="true" id="pass" class="form-control">
                                    </div>
                                </div>
                                <input type="submit" value="Login" class="btn btn-primary text-right">
                            </form>
                        </div>
                        </div>
                    </div>
                    <!-- end single slide -->
                </div>
                <!-- End Wrapper for slides -->
                
            </div>
        </section>
