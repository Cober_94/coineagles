    <body id="body">
	
		<!-- preloader -->
		<div id="preloader" style="text-align: center;">
			<img src="img/pre-inicio.gif"  class="col-lg-3 col-md-3 col-sm-6">
		</div>
		<!-- end preloader -->

        <!-- ======================== Navigation ==================================== -->
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">
							<img src="img/logo_todo.png" alt="Brandi">
						</h1>
					</a>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="#body">Home</a></li>
                        <li><a href="#About">About Us</a></li>
                        <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#loginModal">Log in </a></li>
                        <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#singModal">New Account</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
            </div>
        </header>
        <!--=========================== End  Navigation ==================================== -->
			
        <!--================================== Home Slider==================================== -->
		<section id="slider">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			
							
				
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					
					<!-- single slide -->
					<div class="item active" style="background-image: url(img/baner4.jpg);">
						<div class="carousel-caption">
							<h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated">About Us<span></span></h2>
							<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/Coin Eagle!!</span> All about us.</h3>
							<p data-wow-duration="1000ms" class="wow slideInRight animated">Mission, Vision and Customer Service.</p>
							
							<ul class="social-links text-center">
								<li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- End Wrapper for slides -->
				
			</div>
		</section>
        <!--========================== End Home SliderEnd ==================================== -->
		
        <!--===================================== Features ==================================== -->		
		<section id="About" class="features">
			<div class="container">
				<div class="row">
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
						<h2>All About Us</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					<!--start mission-->
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- img mission -->
						<div class="col-md-6 wow fadeInLeft" data-wow-duration="500ms">
							<div class="service-item">
								<div class="col-lg-12 col-md12- col-sm-6">
									<img src="img/somos.png" width="100%">
								</div>
							</div>
						</div>
						<!-- end img mission -->
						
						<!-- content mission -->
						<div class="col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-users fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3>Mission.</h3>
									<br>
									<h3><span class="color">Coin Eagle</span></h3><br>
									<p>
										<h4 class="sec-title" style="text-align: justify;">.info brings together a community of traders and qualified investors dedicating to creating a beneficial and profitable environment for our members. Now you can turn your money-making dreams into a reality!
											<br><br><br>
											Our mission is to make cryptocurrency accessible to anyone anywhere in the world and to offer an experience that both yields a profit and is easy to participate
											<br><br><br>
											<div style="text-align: right;">
												<img src="img/logo_we_are.png" >
											</div>
										</h4>
									</p>
								</div>
							</div>
							<!-- end content mission -->
						</div>
						<!--end mission-->
						<!--start vision-->
						<div class="col-lg-12 col-md-12 col-sm-12">	
						<div class="devider-1" style="text-align: center;"><i class="fa fa-heart-o fa-lg"></i></div>					
							<!-- content vision -->
							<div class="col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
								<div class="service-item">
									<div class="service-icon">
										<i class="fa fa-users fa-2x"></i>
									</div>
									
									<div class="service-desc">
										<h3>Vision.</h3>
										<br>
										<h3><span class="color">Coin Eagle</span></h3><br>
										<p>
											<h4 class="sec-title" style="text-align: justify;">.info brings together a community of traders and qualified investors dedicating to creating a beneficial and profitable environment for our members. Now you can turn your money-making dreams into a reality!
												<br><br><br>
												Our visión is to be the world leader in cryptocurrency trading by offering a unique program an unmatched customer service
												<br><br><br>
												<div style="text-align: left;">
													<img src="img/logo_we_are.png" >
												</div>
											</h4>
										</p>
									</div>
								</div>
							</div>
							<!-- end vision-->
							<!-- img vision -->
							<div class="col-md-6 wow fadeInLeft" data-wow-duration="500ms">
								<div class="service-item">
									<div class="col-lg-12 col-md12- col-sm-6">
										<img src="img/somos.png" width="100%">
									</div>
								</div>
							</div>
							<!-- end img vision -->
						</div>
						<!--end vision-->
						<!--start customers services-->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="devider-1" style="text-align: center;"><i class="fa fa-heart-o fa-lg"></i></div>	
							<div class="col-md-12 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
								<div class="service-item">
									<div class="service-icon">
										<i class="fa fa-users fa-2x"></i>
									</div>
									
									<div class="service-desc">
										<h3>Customers Services.</h3>
										<br>
										<h3><span class="color">Coin Eagle</span></h3><br>
										<p>
											<h4 class="sec-title" style="text-align: justify;">.info brings together a community of traders and qualified investors dedicating to creating a beneficial and profitable environment for our members. Now you can turn your money-making dreams into a reality!
												<br><br><br>
												
												<br><br><br>
												<div style="text-align: left;">
													<img src="img/logo_we_are.png" >
												</div>
											</h4>
										</p>
									</div>
								</div>
							</div>
						</div>
						<!--end customers services-->
					</div>
					<br>
						<br>
						<br>
						<div style="text-align: center;">
							<a href="index.html" class="btns btn-1"><i class="fa fa-arrow-left"></i> Regresar</a>
						</div>
				</div>
			</div>
		</section>		
        <!--==================================== End Features==================================== -->

		<!--=========================== footer ==================================== -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
						<div class="footer-single">
							<img src="img/footer-logo1.png" alt="">
							<p style="text-align: justify;">so if You want to begin investing, but you do not know how? coineagle.info will assist you to fulfill your investment potential. We provide an encouraging collaboration that will cause individual investor the income from the company's trading activities.</p>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="footer-single">
							<h6>Subscribe </h6>
							<form action="#" class="subscribe">
								<input type="text" placeholder="email" name="subscribe" id="subscribe">
								<input type="submit" value="&#8594;" id="subs">
							</form>
							<p>The most genuine transactions and supreme earnings from the allocation of funds are supported by the minimum level of ranches and financial contingencies.</p>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<div class="footer-single">
							<h6>Explore</h6>
							<ul>
								<li><a href="about.html">About Us</a></li>
								<li><a href="investment.html">Subscriptions</a></li>
								<li><a href="index.html">Home</a></li>
							</ul>
						</div>
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
						<div class="footer-single">
							<h6>Support</h6>
							<ul>
								<li><a href="support.html">Contact Us</a></li>
								<li><a href="support.html">send Message</a></li>
								<li><a href="questions.html">Frequent Questions</a></li>
							</ul>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © 2019 <a href="index.html">Coin Eagle</a>. All rights reserved.<a href=""></a>
						</p>
					</div>
				</div>
			</div>
		</footer>
		<!--=========================== modal login  ==================================== -->
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    	
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                        <h4 class="modal-title"><img src="img/logo_we_are.png"></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="row">
                    		<h4 class="" style="text-align: center; color: #2d8fd0">Login with your username and password</h4>
                    		<img src="">
                    		<form name="login">

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
                    					<div class="service-icon">
											<i class="fa fa-user fa-2x"></i>
										</div>
                    				</div>
                    				<div class="col-lg-10 col-md-10 col-sm-10">
                    					<label>Username:</label>
                    					<input type="text" name="user_name" required="true" id="user_name" class="form-control">
                    				</div>
                    			</div>
                    			<br>
                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
                    					<div class="service-icon">
											<i class="fa fa-key fa-2x"></i>
										</div>
                    				</div>	
                    				<div class="col-lg-10 col-md-10 col-sm-10">
                    					<label>Password:</label>
                    					<input type="password" name="pass" required="true" id="pass" class="form-control">
                    				</div>
                    			</div>

                    		</form>
                    	</div>              	
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="#">Login</a>
                    </div>
                </div>
            </div>
        </div>
        <!--=========================== End Modal  ==================================== -->

        <!--=========================== modal new acount  ==================================== -->
		<div class="modal fade" id="singModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    	
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <h4 class="modal-title"><img src="img/logo_we_are.png"></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="row">
                    		<h4 class="" style="text-align: center; color: #2d8fd0">Create new Account in Coin Eagle!</h4>
                    		<br>
                    		<h6 style="text-align: center; color: #2d8fd0">Fields containing an 'X' are required</h6>
                    		
                    		<form name="login">
                    			<br>
                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-user"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>First Name: <strong>x</strong></label>
	                    					<input type="text" name="user_name" required="true" id="user_name" class="form-control">
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-user"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Last Name: <strong>x</strong></label>
	                    					<input type="text" name="user_name" required="true" id="user_name" class="form-control">
	                    				</div>
	                    			</div>
                    			</div>

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<br>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-book"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Account type: <strong>x</strong></label>
	                    					<select name="account_type" id="account_type" class="form-control" required="true">
	                    						<option value="">[--Select an option--]</option>
	                    						<option value="1">Bronze 5%</option>
	                    						<option value="2">Silver 7%</option>
	                    						<option value="3">Gold 9%</option>
	                    					</select>
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
                    					<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-money"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Account amount: <strong>x</strong></label>
	                    					<input type="number" name="amount" id="amount" class="form-control" required="true">
	                    				</div>
                    				</div>
                    			</div>

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<br>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-phone"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Phone number: <strong>x</strong></label>
	                    					<input type="text" name="phone" id="phone" required="true" class="form-control">
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-credit-card"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Identified: </label>
	                    					<input type="text" name="identified" id="identified" class="form-control">
	                    				</div>
	                    			</div>
	                    			
                    			</div>

                    			<div class="col-lg-12 col-md-12 col-sm-12">
                    				<br>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-flag"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>Country: <strong>x</strong></label>
	                    					<select name="country" id="country" class="form-control" required="true">
	                    						<option value="">[--Select an option--]</option>
	                    						<option value="1">El Salvador</option>
	                    						<option value="2">Guatemala</option>
	                    						<option value="3">Honduras</option>
	                    						<option value="4">United States</option>
	                    						<option value="5">Mexico</option>
	                    					</select>
	                    				</div>
                    				</div>
                    				<div class="col-lg-6 col-md-6 col-sm-12">
	                    				<div class="col-lg-2 col-md-2 col-sm-2"><br>
	                    					<div class="service-icon">
												<i class="fa fa-flag"></i>
											</div>
	                    				</div>
	                    				<div class="col-lg-10 col-md-10 col-sm-10">
	                    					<label>City: </label>
	                    					<select name="city" id="city" class="form-control" >
	                    						<option value="">[--Select an option--]</option>
	                    						<option value="1">San Salvador</option>
	                    						<option value="2">New Guatemala</option>
	                    						<option value="3">Tegucigalpa</option>
	                    						<option value="4">Texas</option>
	                    						<option value="5">New Mexico</option>
	                    					</select>
	                    				</div>
	                    			</div>
	                    			<br><br><br><br>
                    			</div>
                    			
                    			<div class="modal-footer">
                        			<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        				<input type="submit" name="enviar" id="enviar" value="Create Account" class="btn btn-primary">
                    			</div>
                    		</form>
                    	</div>              	
                    </div>
                </div>
            </div>
        </div>
        <!--=========================== end modal new acount  ==================================== -->
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>