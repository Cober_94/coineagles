-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2019 a las 18:37:16
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coin_eagle`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id_cuenta` int(11) NOT NULL,
  `codigo_cienta` varchar(10) NOT NULL,
  `id_user` int(11) NOT NULL,
  `monto` double NOT NULL,
  `estado` int(11) NOT NULL,
  `tipo_cuenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id_cuenta`, `codigo_cienta`, `id_user`, `monto`, `estado`, `tipo_cuenta`) VALUES
(1, 'D5410', 1, 120.03, 1, 1),
(2, 'S5410', 2, 100.03, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id_proceso` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `monto` double NOT NULL,
  `type_proceso` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `mes` int(2) NOT NULL,
  `anio` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id_proceso`, `id_cuenta`, `monto`, `type_proceso`, `fecha`, `hora`, `mes`, `anio`) VALUES
(1, 1, 120.03, 1, '2019-05-10', '09:42:30', 5, 2019),
(2, 2, 120.03, 1, '2019-05-10', '09:00:00', 5, 2019);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`) VALUES
(1, 'user'),
(2, 'admint');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_type_cuneta`
--

CREATE TABLE `tb_type_cuneta` (
  `tb_type` int(11) NOT NULL,
  `type_cuenta` varchar(10) NOT NULL,
  `porcentaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_type_cuneta`
--

INSERT INTO `tb_type_cuneta` (`tb_type`, `type_cuenta`, `porcentaje`) VALUES
(1, 'Bronce', 5),
(2, 'Plata', 7),
(3, 'Oro', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_type_proceso`
--

CREATE TABLE `tb_type_proceso` (
  `id_tipo_proceso` int(11) NOT NULL,
  `type_proceso` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_type_proceso`
--

INSERT INTO `tb_type_proceso` (`id_tipo_proceso`, `type_proceso`) VALUES
(1, 'Ingreso'),
(2, 'Retiro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `id_transaccion` int(11) NOT NULL,
  `id_cuenta_de` int(11) NOT NULL,
  `id_cuenta_a` int(11) NOT NULL,
  `monto` double NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `mes` int(2) NOT NULL,
  `anio` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`id_transaccion`, `id_cuenta_de`, `id_cuenta_a`, `monto`, `fecha`, `hora`, `mes`, `anio`) VALUES
(1, 2, 1, 20, '2019-05-10', '12:06:36', 5, 2091);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `foto` varchar(30) DEFAULT NULL,
  `identificacion` varchar(15) DEFAULT NULL,
  `teelefono` varchar(15) NOT NULL,
  `pais` varchar(20) NOT NULL,
  `cuidad` varchar(30) NOT NULL,
  `pass` varchar(60) NOT NULL,
  `pin` varchar(25) NOT NULL,
  `rol` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `nombre`, `apellidos`, `correo`, `foto`, `identificacion`, `teelefono`, `pais`, `cuidad`, `pass`, `pin`, `rol`, `estado`) VALUES
(1, 'Dennis Ismael ', 'Aviles Goméz', 'desar28@outlook.es', 'Dennis.jpg', '1420036-0', '0000-0000', 'El Salvador', 'San Salvador', '2350', '560D1R0', 1, 1),
(2, 'Samuel Adonai', 'Alvarado Morales', 'samuel562_@gmail.com', 'samuel.jpg', '414010-5', '7810-6310', 'Guatemala', 'Guatemala', '5602', '20DS42Z', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `tipo_cuenta` (`tipo_cuenta`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id_proceso`),
  ADD KEY `id_cuenta` (`id_cuenta`),
  ADD KEY `type_proceso` (`type_proceso`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `tb_type_cuneta`
--
ALTER TABLE `tb_type_cuneta`
  ADD PRIMARY KEY (`tb_type`);

--
-- Indices de la tabla `tb_type_proceso`
--
ALTER TABLE `tb_type_proceso`
  ADD PRIMARY KEY (`id_tipo_proceso`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`id_transaccion`),
  ADD KEY `id_cuenta_de` (`id_cuenta_de`),
  ADD KEY `id_cuenta_a` (`id_cuenta_a`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `rol` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id_proceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_type_cuneta`
--
ALTER TABLE `tb_type_cuneta`
  MODIFY `tb_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tb_type_proceso`
--
ALTER TABLE `tb_type_proceso`
  MODIFY `id_tipo_proceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `id_transaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `cuenta_ibfk_2` FOREIGN KEY (`tipo_cuenta`) REFERENCES `tb_type_cuneta` (`tb_type`);

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `proceso_ibfk_1` FOREIGN KEY (`id_cuenta`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `proceso_ibfk_2` FOREIGN KEY (`type_proceso`) REFERENCES `tb_type_proceso` (`id_tipo_proceso`);

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `transaccion_ibfk_1` FOREIGN KEY (`id_cuenta_de`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `transaccion_ibfk_2` FOREIGN KEY (`id_cuenta_a`) REFERENCES `cuenta` (`id_cuenta`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
